package com.app.am.myapplication;

import android.support.annotation.NonNull;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

/**
 * Created by kalyani on 4/29/2017.
 */

public class FirebaseConfig {

    private static final String WELCOME_MESSAGE_KEY = "welcome_message";
    private static final String SET_ONE_KEY = "set_one";
    private static final String SET_TWO_KEY = "set_two";
    private static final String SET_THREE_KEY = "set_three";
    private static final String SET_FOUR_KEY = "set_four";

    private FirebaseRemoteConfig mFirebaseRemoteConfig;

    private static final FirebaseConfig ourInstance = new FirebaseConfig();

    public static FirebaseConfig getInstance()
    {
        return ourInstance;
    }

    private FirebaseConfig() {
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults);
        // [END set_default_values]

        //fetchWelcome();


    }



}
