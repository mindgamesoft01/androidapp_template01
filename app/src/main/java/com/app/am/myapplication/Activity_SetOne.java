package com.app.am.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.am.myapplication.adapter.DataAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class Activity_SetOne extends Activity {
    // Remote Config keys
//    private static final String LOADING_PHRASE_CONFIG_KEY = "loading_phrase";
    private static final String WELCOME_MESSAGE_KEY = "welcome_message";
//    private static final String WELCOME_MESSAGE_CAPS_KEY = "welcome_message_caps";

    private FirebaseRemoteConfig mFirebaseRemoteConfig;
    //TextView decleration
    TextView textView;
    InterstitialAd interstitialAd;

    LinearLayout layout, strip, layout1, strip1;
    AdClass ad = new AdClass();
    TextView Title1;



    public ArrayList<String> images_json;
    //URLs of the images to load in the gallery1



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //assigning the layout
        setContentView(R.layout.activity_main);
        /*Get the JSON data from firebase database*/
        //AdMob();
        images_json = new ArrayList<String>();
        Title1 = (TextView) findViewById(R.id.heading);
        Title1.setText(singleton_images.getInstance().getApp_name());



        layout = (LinearLayout) findViewById(R.id.admob);
        strip = ad.layout_strip(this);
        layout.addView(strip);
        ad.AdMobBanner(this);
        images_json = singleton_images.getInstance().get_set_images(0);

        initViews();



    }


    private void initViews() {
        //registering the textview
        textView=(TextView)findViewById(R.id.heading);
    //recycler view registration
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.card_recycler_view);
        //when you are adding or removing items in the RecyclerView and that doesn't change it's height or the width.
        recyclerView.setHasFixedSize(true);
        //assigning gridlayout to the recyclerview in two columns
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(),2);
        recyclerView.setLayoutManager(layoutManager);

      //Assigning the ulrs to the arraylist
        //ArrayList image_Urls = prepareData();
        //sending the array list to the dataadapter class
        DataAdapter adapter = new DataAdapter(getApplicationContext(), images_json);
        //set the adapter to the recycler view
        recyclerView.setAdapter(adapter);

        // Get Remote Config instance.
        // [START get_remote_config_instance]
//        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
//       // [END get_remote_config_instance]
//
//        // Create a Remote Config Setting to enable developer mode, which you can use to increase
//        // the number of fetches available per hour during development. See Best Practices in the
//        // README for more information.
//        // [START enable_dev_mode]
//        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
//                .setDeveloperModeEnabled(BuildConfig.DEBUG)
//                .build();
//        mFirebaseRemoteConfig.setConfigSettings(configSettings);
//        // [END enable_dev_mode]
//
//        // Set default Remote Config parameter values. An app uses the in-app default values, and
//        // when you need to adjust those defaults, you set an updated value for only the values you
//        // want to change in the Firebase console. See Best Practices in the README for more
//        // information.
//        // [START set_default_values]
//        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults);
//        // [END set_default_values]
//
//        fetchWelcome();

    }
//    private void fetchWelcome(){
//        textView.setText(mFirebaseRemoteConfig.getString(WELCOME_MESSAGE_KEY));
//        long cacheExpiration = 3600; // 1 hour in seconds.
//        // If your app is using developer mode, cacheExpiration is set to 0, so each fetch will
//       // retrieve values from the service.
//        if (mFirebaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
//            cacheExpiration = 0;
//        }
//        mFirebaseRemoteConfig.fetch(cacheExpiration)
//                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
//                    @Override
//                    public void onComplete(@NonNull Task<Void> task) {
//                        if (task.isSuccessful()) {
//                            Toast.makeText(Activity_SetOne.this, "Fetch Succeeded",
//                                    Toast.LENGTH_SHORT).show();
//
//                            // After config data is successfully fetched, it must be activated before newly fetched
//                            // values are returned.
//                            mFirebaseRemoteConfig.activateFetched();
//                        } else {
//                            Toast.makeText(Activity_SetOne.this, "Fetch Failed",
//                                    Toast.LENGTH_SHORT).show();
//                        }
//                        displayWelcomeMessage();
//                    }
//                });
//        // [END fetch_config_with_callback]
//    }

    /**
     * Display a welcome message in all caps if welcome_message_caps is set to true. Otherwise,
     * display a welcome message as fetched from welcome_message.
     */
    // [START display_welcome_message]
//    private void displayWelcomeMessage() {
//        // [START get_config_values]
//        String welcomeMessage = mFirebaseRemoteConfig.getString(WELCOME_MESSAGE_KEY);
//        // [END get_config_values]
//        if (mFirebaseRemoteConfig.getBoolean(WELCOME_MESSAGE_KEY)) {
//            textView.setAllCaps(true);
//        } else {
//            textView.setAllCaps(false);
//        }
//        textView.setText(welcomeMessage);
//    }
// [END display_welcome_message]






    public void AdMob(){
        AdRequest adRequest = new AdRequest.Builder().build();
        interstitialAd=new InterstitialAd(this);
        interstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                Log.d("AD-EVENT","onAdClosed.");
                Toast.makeText(Activity_SetOne.this, "onAdClosed", Toast.LENGTH_SHORT).show();


            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                Log.d("AD-EVENT","onAdFailedToLoad.");
                Toast.makeText(Activity_SetOne.this, "onAdFailedToLoad", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLeftApplication() {
                super.onAdLeftApplication();
                Log.d("AD-EVENT","onAdLeftApplication.");
                Toast.makeText(Activity_SetOne.this, "onAdLeftApplication", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
                Log.d("AD-EVENT","onAdOpened");
                Toast.makeText(Activity_SetOne.this, "onAdOpened", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                interstitialAd.show();
                Log.d("AD-EVENT","onAdLoaded.");
                Toast.makeText(Activity_SetOne.this, "onAdLoaded", Toast.LENGTH_SHORT).show();
            }
        });
        interstitialAd.loadAd(adRequest);
    }

}
