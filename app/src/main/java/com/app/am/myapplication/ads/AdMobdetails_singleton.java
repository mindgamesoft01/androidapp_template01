package com.app.am.myapplication.ads;

import android.app.ActionBar;
import android.content.Context;
import android.util.Log;
import android.widget.LinearLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Ajay on 5/15/2017.
 */

public class AdMobdetails_singleton {
    private static final AdMobdetails_singleton ourInstance = new AdMobdetails_singleton();

    public static AdMobdetails_singleton getInstance() {
        return ourInstance;
    }

    private AdMobdetails_singleton() {
    }
    Context c;
    LinearLayout layout, layout1;
    // AdMOb
    AdView adView;
    String AD_UNIT_ID = "ca-app-pub-3940256099942544/6300978111";

    public JSONObject getAdmob_details() {
        return admob_details;
    }

    public void setAdmob_details(JSONObject admob_details) {
        this.admob_details = admob_details;
    }

    public JSONObject admob_details;


    AdView adView1;
    String AD_UNIT_ID1 = get_admob_id(0,"banner");

    InterstitialAd interstitial;
    String AD_UNIT_ID_full_page = "";

    InterstitialAd interstitial1;
    String AD_UNIT_ID_full_page1 = "";


    //for banner: ca-app-pub-3940256099942544/6300978111
    //for interstitial: ca-app-pub-3940256099942544/1033173712



	/*
	 * // RevMob RevMob revmob; RevMobBanner banner; // StartApp private
	 * StartAppAd startAppAd;
	 *
	 * // MM Ads
	 *
	 * MM mm; MMBestApps mmb
	 */;

    public LinearLayout layout_strip(Context context) {

        c = context;
        Log.d("TASG", "entered layout code");

        layout = new LinearLayout(c);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

        layout.setLayoutParams(params);
        layout.setOrientation(LinearLayout.HORIZONTAL);

        return layout;
    }

    public void AdMobBanner(Context c) {

        adView = new AdView(c);
        adView.setAdSize(AdSize.SMART_BANNER);
        adView.setAdUnitId(AD_UNIT_ID);

        layout.addView(adView);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

    }

    public void AdMobBanner1(Context c) {

        adView1 = new AdView(c);
        adView1.setAdSize(AdSize.SMART_BANNER);
        adView1.setAdUnitId(AD_UNIT_ID1);

        layout.addView(adView1);

        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);

    }

    public void AdMobInterstitial(Context c) {
        interstitial = new InterstitialAd(c);
        interstitial.setAdUnitId(AD_UNIT_ID_full_page);

        // Create ad request.
        AdRequest adRequest = new AdRequest.Builder().build();
        // Begin loading your interstitial.
        interstitial.loadAd(adRequest);
        interstitial.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // TODO Auto-generated method stub
                super.onAdLoaded();
                interstitial.show();
            }
        });

    }

    public void AdMobInterstitial1(Context c) {
        interstitial1 = new InterstitialAd(c);
        interstitial1.setAdUnitId(AD_UNIT_ID_full_page1);

        // Create ad request.
        AdRequest adRequest = new AdRequest.Builder().build();
        // Begin loading your interstitial.
        interstitial1.loadAd(adRequest);
        interstitial1.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // TODO Auto-generated method stub
                super.onAdLoaded();
                interstitial1.show();
            }
        });

    }

    /*---------------------------------------------------------------------------*
  *
   *@param index - index of the menu item
  * @param adunit - Values - interstitial/banner
  * @return
  * sample => get_admob_id(0, "interstitial"),get_admob_id(0, "banner")
  **---------------------------------------------------------------------------*/
    public String get_admob_id(int index, String adunit) {
        String adunit_id = "";
        try {
            JSONArray tmp = admob_details.getJSONArray(adunit);
            adunit_id = tmp.getString(index);
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("get_admob_id","Unable to get the admob ad id.");
        }

        return adunit_id;
    }

}
