package com.app.am.myapplication;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class Launch_ActivityTest {

    @Rule
    public ActivityTestRule<Activity_01> mActivityTestRule = new ActivityTestRule<>(Activity_01.class);

    @Test
    public void launch_ActivityTest() {
        ViewInteraction appCompatImageView = onView(
                allOf(withId(R.id.btn1), isDisplayed()));
        appCompatImageView.perform(click());

        ViewInteraction appCompatImageView2 = onView(
                allOf(withId(R.id.btn1), isDisplayed()));
        appCompatImageView2.perform(click());

        ViewInteraction imageButton = onView(
                allOf(withContentDescription("Interstitial close button"), isDisplayed()));
        imageButton.perform(click());

        ViewInteraction imageView = onView(
                allOf(withId(R.id.img1), isDisplayed()));
        imageView.perform(click());

    }

}
