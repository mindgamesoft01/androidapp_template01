package com.app.am.myapplication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.am.myapplication.ads.AdMobdetails_singleton;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import org.json.JSONArray;
import org.json.JSONObject;

public class Activity_00 extends Activity {

	LinearLayout layout, strip, layout1, strip1;
	AdMobdetails_singleton ad = AdMobdetails_singleton.getInstance() ;
	ProgressBar spinner;
	Boolean network;
	Context c;
	PopupWindow pop;
	InterstitialAd interstitialAd;
	String interstitial;
	AdMobdetails_singleton AdMob=AdMobdetails_singleton.getInstance();
	String INTERSTITIAL_KEY="interstitial";
	TextView app_name;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_00);
		c=getApplicationContext();

		app_name = (TextView) findViewById(R.id.app_name);

		layout = (LinearLayout) findViewById(R.id.admob);
		strip = ad.layout_strip(this);
		layout.addView(strip);
		ad.AdMobBanner(this);
		pop=new PopupWindow(this);
		interstitial=AdMob.get_admob_id(4,INTERSTITIAL_KEY);

//		layout1 = (LinearLayout) findViewById(R.id.admob1);
//		strip1 = ad.layout_strip(this);
//		layout1.addView(strip1);
//		ad.AdMobBanner1(this);
		network=AppStatus.getInstance(c).isOnline();
		Log.d("Network Checking :",network.toString());
		networkChecking();

		getJSONdata();
	}

	public void gallery(View v) {

		Intent i = new Intent(Activity_00.this, Activity_01.class);
		i.putExtra("ads",true
		);

		startActivity(i);

	}

	public void mywork(View v) {
		//ad.AdMobInterstitial(SecondActivity.this);
		Intent i = new Intent(
				Intent.ACTION_VIEW,
				Uri.parse("https://play.google.com/store/apps/developer?id=Andromida%20apps&hl=en"));

		startActivity(i);

	}
	public void favourite(View v) {
		//ad.AdMobInterstitial1(SecondActivity.this);
		Intent i = new Intent(Activity_00.this, MyImages.class);

		startActivity(i);


	}


	public void rate(View v) {
		//ad.AdMobInterstitial1(SecondActivity.this);
		
		Intent i = new Intent(
				Intent.ACTION_VIEW,
				Uri.parse("https://play.google.com/store/apps/details?id=com.mn.app4.hair.hairjwellerygallery"));

		startActivity(i);
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (keyCode == KeyEvent.KEYCODE_BACK) {

			Intent intent = new Intent(Intent.ACTION_MAIN);
			intent.addCategory(Intent.CATEGORY_HOME);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
		}

		return super.onKeyDown(keyCode, event);
	}

	public void getJSONdata(){

		RequestQueue mRequestQueue;

// Instantiate the cache
		Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024); // 1MB cap

// Set up the network to use HttpURLConnection as the HTTP client.
		Network network = new BasicNetwork(new HurlStack());

// Instantiate the RequestQueue with the cache and network.
		mRequestQueue = new RequestQueue(cache, network);

// Start the queue
		mRequestQueue.start();

		String url = "https://myapplication2-6a480.firebaseio.com/images.json";


		JsonObjectRequest jsObjRequest = new JsonObjectRequest
				(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						Log.d("JSON", response.toString());
						JSONArray app_images = new JSONArray();
						try {
							singleton_images singleton = singleton_images.getInstance();
							if (response.getString("app_mode").equals("test")) {
								app_images = response.getJSONArray("test_images");
							} else  if (response.getString("app_mode").equals("prod")){
								app_images = response.getJSONArray("prod_images");
							}


							JSONArray ad_details = response.getJSONArray("ad_token");
							JSONObject admob_details = response.getJSONObject("admob_details");
							/*Set the Ad details in Admob*/

							AdMobdetails_singleton.getInstance().setAdmob_details(admob_details);
							app_name.setText(response.getString("app_name"));
							singleton.setInterstitial_timer( response.getInt("interstitial_timer"));
							singleton.setApp_name(response.getString("app_name"));
							singleton.setAdmob_details(admob_details);
							singleton.setImages_json(app_images);
							singleton.setAd_details(ad_details);


						}
						catch (Exception e){
							e.printStackTrace();
						}


					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						Log.e("JSON", error.toString());
						// TODO Auto-generated method stub

					}
				});

// Add the request to the RequestQueue.
		mRequestQueue.add(jsObjRequest);
	}
	public  void networkChecking(){
		if (AppStatus.getInstance(c).isOnline()==Boolean.FALSE) {
			Log.d("Network Checking :",network.toString());


			//Toast.makeText(getApplicationContext(),"No NetWork ",Toast.LENGTH_SHORT).show();
		}
		if (AppStatus.getInstance(c).isOnline()==Boolean.TRUE) {
			Log.d("Network Checking :",network.toString());
//			Intent i = new Intent(Activity_00.this,Pop.class);
//			startActivity(i);
			Toast.makeText(getApplicationContext(),"No Network ",Toast.LENGTH_LONG).show();
		}

	}


}
