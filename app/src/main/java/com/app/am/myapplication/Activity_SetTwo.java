package com.app.am.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.am.myapplication.adapter.DataAdapter;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class Activity_SetTwo extends Activity {
    // Remote Config keys
//    private static final String LOADING_PHRASE_CONFIG_KEY = "loading_phrase";
    private static final String WELCOME_MESSAGE_KEY = "welcome_message";
//    private static final String WELCOME_MESSAGE_CAPS_KEY = "welcome_message_caps";

    private FirebaseRemoteConfig mFirebaseRemoteConfig;
    //TextView decleration
    TextView textView;
    TextView Title3;
    InterstitialAd interstitialAd;
    LinearLayout layout, strip, layout1, strip1;
    AdClass ad = new AdClass();


    public ArrayList<String> images_json;
    //URLs of the images to load in the gallery1



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //assigning the layout
        setContentView(R.layout.activity_main);
        /*Get the JSON data from firebase database*/
        //AdMob();
        layout = (LinearLayout) findViewById(R.id.admob);
        Title3 = (TextView) findViewById(R.id.heading);
        strip = ad.layout_strip(this);
        layout.addView(strip);
        ad.AdMobBanner(this);
        images_json = new ArrayList();
        images_json = singleton_images.getInstance().get_set_images(1);
        Title3.setText(singleton_images.getInstance().getApp_name());

        initViews();}

    private void initViews() {
        //registering the textview
        textView=(TextView)findViewById(R.id.heading);
    //recycler view registration
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.card_recycler_view);
        //when you are adding or removing items in the RecyclerView and that doesn't change it's height or the width.
        recyclerView.setHasFixedSize(true);
        //assigning gridlayout to the recyclerview in two columns
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(),2);
        recyclerView.setLayoutManager(layoutManager);

        DataAdapter adapter = new DataAdapter(getApplicationContext(), images_json);
        //set the adapter to the recycler view
        recyclerView.setAdapter(adapter);



    }











}
